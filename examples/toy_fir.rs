extern crate turbo;

use turbo::{Code, CodeType, b3, ONE, ZERO};
use turbo::encoders::Encoder;

fn main() {
  let toy_code = Code {
    start_state: vec![ZERO, ZERO],
    polys: vec![b3::new([ZERO, ZERO, ONE]),
                b3::new([ONE, ONE, ZERO])],
    code_type: CodeType::FIRCode
  };

  let toy_signal = vec![ONE, ONE, ONE];
  let encoded_signal = toy_code.encode(&toy_signal);

  println!("input signal (len {}): ", toy_signal.len());
  for bit in toy_signal {
    print!("{}", bit);
  }
  println!("");
  println!("encoded signal (len {}):", encoded_signal.len());
  for bit in encoded_signal {
    print!("{}", bit);
  }
}
