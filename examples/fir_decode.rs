extern crate turbo;

use turbo::{Code, CodeType, b3, ONE, ZERO};
use turbo::encoders::Encoder;
use turbo::viterbi::MLDecoder;

fn main() {
  let toy_code = Code {
    start_state: vec![ZERO],
    polys: vec![b3::new([ZERO, ZERO, ONE]),
                b3::new([ONE, ONE, ZERO])],
    code_type: CodeType::FIRCode
  };

  let toy_signal = vec![ONE, ONE, ONE];
  let encoded_signal = toy_code.encode(&toy_signal);

  let state_tree = toy_code.estimate_states(&toy_signal).0;

  for node in state_tree.raw_nodes() {
    println!("{:?}", node);
  }
}
