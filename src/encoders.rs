use {Code, CodeType, b3, b1};

/// Trait for convolutional encoders.
pub trait Encoder {
  /// Encoder implementation for convolutional codes. Matches on code type,
  /// computing FIR and IIR codes separately.
  ///
  /// # Examples
  ///
  /// ## FIR encoder
  ///
  /// ```
  /// use turbo::{Code, CodeType, b3, ONE, ZERO};
  /// use turbo::encoders::Encoder;
  /// let toy_code = Code {
  ///   start_state: vec![ZERO, ZERO],
  ///   polys: vec![b3::new([ZERO, ZERO, ONE]),
  ///               b3::new([ONE, ONE, ZERO])],
  ///   code_type: CodeType::FIRCode
  /// };
  ///
  /// let toy_signal = vec![ONE, ONE, ONE];
  /// let encoded_signal = toy_code.encode(&toy_signal);
  ///
  /// assert_eq!(encoded_signal, vec![ZERO, ONE, ZERO, ZERO, ONE, ZERO]);
  ///
  /// ```
  ///
  /// ## IIR encoder
  /// 
  /// ```
  /// use turbo::{Code, CodeType, b3, ONE, ZERO};
  /// use turbo::encoders::Encoder;
  /// let toy_code = Code {
  ///   start_state: vec![ZERO, ZERO],
  ///   polys: vec![b3::new([ZERO, ZERO, ONE]),
  ///               b3::new([ONE, ONE, ZERO])],
  ///   code_type: CodeType::IIRCode
  /// };
  ///
  /// let toy_signal = vec![ONE, ONE, ONE];
  /// let encoded_signal = toy_code.encode(&toy_signal);
  ///
  /// assert_eq!(encoded_signal, vec![ZERO, ONE, ZERO, ZERO, ZERO, ONE]);
  /// ```
  fn encode(&self, signal: &Vec<b1>) -> Vec<b1>;
}

impl Encoder for Code {  
  fn encode(&self, signal: &Vec<b1>) -> Vec<b1> {
    match self.code_type {
      CodeType::FIRCode => encode_fir(self, signal),
      CodeType::IIRCode => encode_iir(self, signal)
    }
  }
}

fn encode_fir(code: &Code, signal: &Vec<b1>) -> Vec<b1> {
  signal.iter().scan(code.start_state.clone(), |state, sig_bit| {
    state.push(*sig_bit);

    // TODO remove this allocation (collect call)
    let adder_outputs: Vec<_> =
      code.polys.iter().map(|code_poly| {
        poly_add(*code_poly, &state)
      }).collect();

    state.remove(0);    
    Some(adder_outputs)
  }).flat_map(|x| x).collect()
}

fn encode_iir(code: &Code, signal: &Vec<b1>) -> Vec<b1> {
  signal.iter().scan(code.start_state.clone(), |state, sig_bit| {
    // Put the received bit on the end for code computations
    state.push(*sig_bit);
    
    // TODO remove this allocation (collect call)
    let adder_outputs: Vec<_> =
      code.polys.iter().map(|code_poly| {
        poly_add(*code_poly, &state)
      }).collect();

    // Pull the received bit off since this is an IIR code
    state.pop();

    // Swap the number of elements on the end of the state as there are adder
    // outputs
    for out_ind in 0..(adder_outputs.len()) {
      state.remove(0);
      state.push(adder_outputs[out_ind]);
    }
    
    Some(adder_outputs)
  }).flat_map(|x| x).collect()
}

// XOR addition functions on sections of the signal or output weighted by the
// code polynomial.
fn poly_add(code_vec: b3, sig_arr: &[b1]) -> b1 {
  sig_arr.iter().rev().zip(code_vec.bits().iter())
                      .map(|(&sig, &code)| sig & code)
                      .fold(b1(false), |accum, val| accum ^ val)
}

