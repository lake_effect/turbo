use encoders::Code;

pub trait MLDecoder {}
pub trait MAPDecoder {}

impl<T> MLDecoder for FIRCode<T> {}
impl<T> MAPDecoder for FIRCode<T> {}
