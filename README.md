# turbo

![cargo version badge](https://img.shields.io/crates/v/turbo.svg)

## is

This is a library
for
[convolutional coding](https://en.wikipedia.org/wiki/Convolutional_code). It's
not finished at all.

## going to be

### 1.0

- [x] Basic convolutional encoders
- [ ] Viterbi decoder
- [ ] BCJR decoder
- [ ] turbo encoder/decoder

### 2.0

- [ ] Code puncturing
- [ ] Full parallelization of existing codecs
- [ ] Bitstream implementations
  - [ ] Encoders
  - [ ] Viterbi decoder
  - [ ] BCJR decoder
  - [ ] Turbo
